
function callMe(event) {
    event.stopPropagation();
    alert('Pozvao sam');
}

var link = document.querySelector('a');
link.onclick = function(e) {
    e.preventDefault();
    e.stopPropagation();

    link.innerText = 'Web stranica je offline!!';
}

var changeBg = document.querySelector('#change');
changeBg.addEventListener('click', ChangeBG);


function ChangeBG(e) {
    e.preventDefault();
    e.stopPropagation();

    this.style.backgroundColor = '#ccc';
    changeBg.removeEventListener('click', ChangeBG);
    console.log(this);
}