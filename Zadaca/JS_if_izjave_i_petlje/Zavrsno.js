/*
1. Napišite while petlju koja će ispisivati brojeve od 3 do 20, osim onih djeljivih sa 9.

2. Koja je vrijednost isprintana u konzoli?
*/

var number = 0;
while (number <= 20) {
    if(number < 3) {
        number++;
        continue;
    }
    if(number % 9 === 0) {
        number++;
    }
    else
    {
        console.log(number);
        number++;
    }
}

var varOne = 0;
for (i = 0; i < 4; i++){
    for (j = 0; j < 4; j++){
        varOne++;
    }
}
console.log(varOne);
// isprintana vrijednost u konzoli je 16