/*
Koristeći metode rada nad stringovima iz zadanog stringa:
  1. Spremite duljinu stringa u varijablu.
  2. Izdvojite riječ 'sit' u zasebnu varijablu.
  3. Zamijenite riječ 'amet' sa riječi 'elit'.
  4. Konkatenirajte u novu varijablu zadani string sa stringom 'consectetur adipiscing elit', sa zarezom izmedu.
  5. Konvertirajte sve riječi u orginalnom stringu u velika slova
  6. Maknite počenu prazninu u stringu
  7. Nađite slovo na poziciji 12
*/

var text = ' Lorem ipsum dolor sit amet';

var stringLength = text.length;
console.log(stringLength);

var sitWord = text.substring(22,19);
console.log(sitWord);

var replaceWord = text.replace("amet", "elit");
console.log(replaceWord);

var concatenationTest = "consectetur adipiscing" + ", " + "elit";
console.log(concatenationTest);

var allUppercase = text.toUpperCase();
console.log(allUppercase);

var trimText = text.trim();
console.log(trimText);

var findLetter = text.charAt(12); // space
console.log(findLetter);