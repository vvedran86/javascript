// prettier.config.js or .prettierrc.js
module.exports = {
	trailingComma: 'es5',
	tabWidth: 4,
	semi: true,
	singleQuote: true,
	overrides: [
		{
			files: ['*.js'],
			options: {
				tabWidth: 4,
				useTabs: true,
				semi: true,
				singleQuote: true,
				bracketSpacing: true,
				endOfLine: 'crlf',
				parser: 'babel',
			},
		},
	],
};
