/*
U ovoj vježbi koristit ćemo se lodash bibliotekom. Prvi korak je da instalirate lodash paket na lokalno računalo.

Korisnik na našoj stranici kreira password. Želimo osigurati da:
	1. Je svaki znak u passwordu različit od drugih / jedinstven
	2. Su svi znakovi brojke
	3. Password nema više od 10 znakova


Vaš je zadatak:
	1. Napisati funkciju koja provjerava da li su svi znakovi jedinstveni (naputak: pogledajte lodash metode nad nizovima)
	2. Napisati funkciju koja provjerava da li su svi znakovi brojke
	3. Napisati funkciju koja uzima password i skraćuje ju na 10 znakova, ako je broj znakova veći od 10
*/
const _ = require('lodash');

// 1.
let dobro = "12345678901212";
let lose = "1123456";

var jedinstveniZnakovi = function(password) {
    
    let passArr = Array.from(password);
    
    // check if all chars are unique
    let cleanArr = _.uniqWith(password, _.isEqual);
    if(cleanArr.length === passArr.length) {
        console.log(`Lozinka ${passArr} je unikatna.`);
    }
    else {
        console.log(`Lozinka ${passArr} nije unikatna.`);
    }

    // check if all chars are numbers
    passArr.forEach(element => {
        let isANumber = _.isNumber(element);
        
        if(isANumber === true) {
            
        }
        else {
            console.log(`${element} nije broj.`);
        }
    });

    // shrink the password to 10 chars
    if(passArr.length > 10) {
        console.log(`Lozinka ${passArr} je duza od 10 znakova te ce se skratiti na 10 znakova.`);
        let newPass = _.truncate(passArr, {
            'length': 10
        });
        console.log(`Nova skracena lozinka od 10 znakova je ${newPass}`);
    }
    else {
        console.log(`Lozinka je kraca od 10 znakova te se nece skracivati`);
    }
};

jedinstveniZnakovi(dobro);
jedinstveniZnakovi(lose);