/*
1. Napišite JavaScript funkciju koja prihvaća zadani niz kao parametar i pronalazi najdužu riječ unutar niza.
2. Vratite tu riječ iz funkcije i spremite u varijablu.
3. Ispišite varijablu u konzoli.
*/

var words = ['quickest', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog'];

function FindLongestWordInArray(listOfWords) {
    let longestWord = "";
    var wordLength = 0;

    for (let i = 0; i < listOfWords.length; i++) {
        if(listOfWords[i].length > wordLength)
        {
            wordLength = listOfWords[i].length;
            longestWord = listOfWords[i];
        }
    }

    return longestWord + " is the longest with the length of " + wordLength;
};

var word = FindLongestWordInArray(words);

console.log(word);