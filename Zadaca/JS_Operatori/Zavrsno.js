/*
Pratite kod u script.js

1. Koja je vrijednost varijable trueFalse? Čemu služe dvostruki negacijski znakovi?

2., 3. i 4. Koju će vrijednost ispisati console.log? Zašto?
*/

var x = "Lorem ipsum",
    y = 2345,
    z = "2345"
	q = false;
	
// 1. 
var trueFalse = !!"false" == !!"true";
console.log(trueFalse);
// vrijednost varijable ce biti true.
// dvostruki negacijski znakovi sluze za cast-anje u Boolean

// 2. 
console.log(q || x || y || z);
// Konzola ce ispisati Lorem ipsum jer vrijednost q varijable je false pa ce ju preskociti prvoj OR komparaciji
// te posto je jedan uvjet vec zadovoljen, dalje vise nece testirati uvjete

// 3. 
var num = 6;
num--;
console.log(num);
// Konzola ce ispisati 5, jer vrijednost se oduzima s -- operatorom

// 4.
const price = 26.5;    
const taxRate = 0.082;

let totalPrice = price + (price * taxRate);
totalPrice.toFixed(2);

console.log("Total:", totalPrice);
// Konzola ce ispisati 28.673000000000002
// Prvo ce pomnoziti price s taxRate te ce dodati originalnu vrijednost na to
// totalPrice.toFixed(2) je metoda koja vraca vrijednost a posto nije spremljena u novu varijablu
// nece se primjeniti toFixed(2)