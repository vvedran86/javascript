/*
1. Pomoću petlje napravite iteraciju kroz niz i ispisite u konzolu sve elemente niza.
2. Dodajte svoje ime na kraj niza.
3. Koristeći petlju, napravite iteraciju kroz ovaj niz te nakon išto ispišete "Jane" izađite iz petlje.
4. Napišite naredbu za uklanjanje undefined vrijednosti iz niza.
*/

var names = ['John', 'Jane', 'Bob', , 'Mike'];
names.push('Vedran');

names.forEach(name => {
    console.log(name);
});

names.some(function(el) {
    console.log(el);
    return el === "Jane";
});

names.push(undefined);
var removeUndefined = names.filter(function(el) {
    return el != undefined;
});


console.log(removeUndefined);