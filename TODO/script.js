/**
 * Todo object
 * @constructor
 */
class ToDoNote {
    constructor() {
        this.inputTask = document.querySelector('#inputTask');
        this.addButton = document.querySelector('#addTask');
        this.todoList = document.querySelector('#todoList');
        this.saveList = document.querySelector('#saveTaskList');

        //this.addItem = this.addItem.bind(this);
    }

    checkItemsIfExist(checked) {
        const listItems = document.querySelectorAll('#todoList li');
        
        if(listItems.length > 0) {
            this.saveList.disabled = checked;
            (checked != undefined) ? this.setCheckboxCheckedStatus(listItems, checked) : false;
        }
        else {
            this.saveList.disabled = true;
        }
    }

    saveJSONIntoLocalStorage(json) {
        if(Storage != NaN || Storage != undefined) { // cisto blesava provjera za stari browser
            localStorage.tasksList = json
        }
        else
        {
            alert('Imas prestari browser, ajd bok!');
        }
    };

    saveItemList = () => {
        const listItems = document.querySelectorAll('#todoList li');
        const listItemsArr = Array.from(listItems);
        const listItemList = listItemsArr.map((item) => [item.childNodes[0].checked ,item.childNodes[1].nodeValue]);
        const listItemListJSON = JSON.stringify(listItemList);

        // spremanje u Local storage (browser)
        this.saveJSONIntoLocalStorage(listItemListJSON);
    }
    
    markDone = (event, element) => {
        if (event) {
            let item = event.target;
            item.parentNode.classList.toggle('done');
            item.nextSibling.nextSibling.disabled = item.checked;
        } else if (element) {
            element[1] ? element[0].classList.add('done') : false;
            element[0].childNodes[0].checked = element[1]; //param element je array
            element[0].childNodes[2].disabled = element[1];
            console.log(element); // childNodes[2] je gumb
        }
    };

    removeItem = (event) => {
        let removeButton = event.target;
            removeButton.parentNode.remove();

        this.checkItemsIfExist();
    };

    addCheckbox(item) {
        let checkbox = document.createElement('input');
            checkbox.setAttribute('type', 'checkbox');
            checkbox.addEventListener('click', this.markDone);
        item.insertBefore(checkbox, item.firstChild);
    };

    addRemoveButton(item) {
        let removeButton = document.createElement('button');
            removeButton.className = 'removeButton';
            removeButton.innerText = 'remove';
            removeButton.addEventListener('click', this.removeItem);
        item.appendChild(removeButton);
    };

    createItem(text) {
        let item = document.createElement('li');
        item.innerText = text;
        this.addCheckbox(item);
        this.addRemoveButton(item);
        
        return item;
    };

    addItem = () => {
        // Dohvati vrijednost input polja
        let textValue = inputTask.value;

        if(!textValue) {
            alert('Upisi task');
            return;
        }

        // Kreiraj novi element
        let item = this.createItem(textValue);
        // Umetni element u listu
        this.todoList.appendChild(item);
        // Provjeri listu
        this.checkItemsIfExist();
        // Resetiraj input listu
        this.inputTask.value = '';
    };

    loadFromLocalStorage(tasksList) {
        const listItems = JSON.parse(tasksList);
        
        listItems.forEach(element => {
            console.log(element);
            // Kreiraj novi element
            let item = this.createItem(element[1]);
            this.markDone(false, [item, element[0]]);
            this.todoList.appendChild(item);
        });

        this.checkItemsIfExist();
        
    };

    checkLocalStorage() {
        if(localStorage.tasksList) {
            this.loadFromLocalStorage(localStorage.tasksList);
        }
    };

    addListeners() {
        this.addButton.addEventListener('click', this.addItem);
        this.saveList.addEventListener('click', this.saveItemList);
    };

    init() {
        this.addListeners();
        this.checkLocalStorage();
    };
};

const todo = new ToDoNote();
window.addEventListener('load', todo.init());