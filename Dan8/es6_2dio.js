// Arrow functions
// const hello = () => {
//     return "Hello world";
// }

const hello = () => console.log("Hello world!");

const sum = (x, y) => {
    let z = x + y;
    return z;
}

let result = sum(1, 5);
console.log(result);

// Regular function
const regular = function () {
    console.log(this);
}
regular();
window.addEventListener('load', regular);
document.getElementById('regular').addEventListener('click', regular);

// Arrow function
const arrow = () => {
    console.log(this);
}
arrow();
window.addEventListener('load', arrow);
document.getElementById('arrow').addEventListener('click', arrow);


let names = ['Ante', 'Miso', 'Mare'];
// Regular callback function
names.forEach(function(el) {
    console.log(el);
});

// Arrow callback function
names.forEach((item, index) => console.log(index, item));

// Classes
class Hero {
    constructor(name, hp, level) {
        this.name = name;
        this.hp = hp;
        this.level = level;
    }

    greet() {
        return `${this.name} says hello!`;
    };

    calculateHit(hp, isPositiveHit) {
        isPositiveHit ? this.hp += hp : this.hp -= hp;
    }
}

class Wizard extends Hero {
    constructor(name, hp, level, spell) {
        super(name, hp, level);
        this.spell = spell;
    }

}

const hero1 = new Hero('Varg', 100, 1);
hero1.calculateHit(10, true);
console.log(hero1);
const hero2 = new Wizard('Lejon', 150, 2, 'Magic missile');
hero2.calculateHit(10, false);
console.log(hero2);

// Promises
// let promise = new Promise((resolve, reject) => {
//     setTimeout(() => resolve('done'), 1000);
//     setTimeout(() => reject(new Error('Ups, something went wrong')), 1000);
// });

// promise.catch(
//     result => alert(result),
//     error => alert(error)
// );
// console.log(promise);

let promise1 = new Promise((resolve, reject) => {
    setTimeout(() => resolve('done'), 2000);
})
    .finally(() => console.log('Promise ready'))
    .then(result => console.log(result));
console.log(promise1);

// Callback based
// function loadScript(src, callback) {
//     let script = document.createElement('script');
//     script.src = src;
//     script.onload = () => callback(null, script);
//     script.onerror = () => callback(new Error(`Script load error for ${src}`));
    
//     document.head.append(script);
// }

// function handleScript(error, script) {
//     console.log(error);
//     console.log(script);
// }

// loadScript('../Dan7/oop.js', handleScript);

// Promise based
function loadScript(src) {
    return new Promise((resolve, reject) => {
        let script = document.createElement('script');
        script.src = src;
        
        script.onload = () => resolve(script);
        script.onerror = () => reject(new Error(`Script load error for ${src}`));

        document.head.append(script);
    });
}

let newPromise = loadScript('https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.js');

newPromise.then(
    script => alert(`${script.src} is loaded.`),
    error => alert(new Error(`Error: ${error.message}`))
);

newPromise.then(
    script => alert(`Another handler...`),
    error => alert(new Error(`Error: ${error.message}`))
);