// Variables
const cors = 'https://cors-anywhere.herokuapp.com/';
const endpoint = 'http://api.hnb.hr/tecajn/v2';
const params = `?datum-primjene=${moment().format('YYYY-MM-DD')}`;
const completeURL = `${cors}${endpoint}${params}`;
const method = 'GET';

// Functions
const processAll = (data) => {
    console.log(data);
};

const errorHandler = (statusCode) => {
    console.log(`Failed with status code ${statusCode}`);
};

// Regular AJAX call
const makeAjaxCall = function(endpoint, method, callback) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, endpoint, true);
    xhr.send();
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                console.log(`XHR response status: ${xhr.statusText}`);
                let response = JSON.parse(xhr.responseText);
                callback(response);
            } else {
                console.log(`XHR response status: ${xhr.statusText}`);
            }
        } else {
            console.log(`XHR status text ${xhr.statusText} and ready state was stopped at ${xhr.readyState}`);
        }
    }
};

// Promises AJAX call
const makeAjaxPromiseCall = (endpoint, method) => {
    let promise = new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, endpoint, true);
        xhr.send();
        xhr.onreadystatechange = () => {
            if(xhr.readyState === 4) {
                if(xhr.status === 200) {
                    console.log(`XHR response status: ${xhr.statusText}`);
                    let response = JSON.parse(xhr.responseText);
                    resolve(response);
                } else {
                    reject(xhr.status);
                    console.log(`XHR response status: ${xhr.statusText}`);
                }
            } else {
                console.log(`XHR status text ${xhr.statusText} and ready state was stopped at ${xhr.readyState}`);
            }
        }
    });

    return promise;
};

const oncCurrencyClick = function(e) {
    e.preventDefault();
    console.log(this.dataset.valuta);
};

const processCurrencyList = (data) => {
    const currencyList = document.getElementById('currencyList');

    data.forEach(element => {
        const li = document.createElement('li');
        li.textContent = element.drzava;
        li.dataset.valuta = element.valuta;
        currencyList.append(li);
    });

    document.querySelectorAll('#currencyList li').forEach(element => {
        element.addEventListener('click', oncCurrencyClick);
    });;
};

// Method calls
//makeAjaxCall(completeURL, method, processAll);

const onWindowLoad = () => {
    makeAjaxPromiseCall(completeURL, method).then(processCurrencyList, errorHandler);
};

window.addEventListener('load', onWindowLoad);