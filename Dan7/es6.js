'use strict'

// var, let, const
let a = 10;
console.log(a);

if(a == 10) {
        a = 15;
        a = 20;
    console.log(a);
}

console.log(a);

const b = 3.14;
console.log(b);

// Template literals

const name = "Vedran";

const hello = `Moje ime je ${name}`;
console.log(hello);

// Spread operator

function printName(first, second, third) {
    console.log(first, second, third);
}

const names = ['Vedran', 'Ivan', 'David', 'Pero'];
printName(...names);

// Rest operator

function printNames(...names) {
    console.log(names);
}
printNames(...names);

// Primjeri

function xa() {
    console.log('Globalna xa funkcija');
}

xa();

if(true) {
    function xa() {
        console.log('Block xa funkcija');
    }
    xa();
}

function xa() {
    console.log('Block xa funkcija kasnije definirana');
}

xa();