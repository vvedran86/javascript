
var person = {
    name: "John",
    lastname: "Doe"
}

console.log(person);

function Person() {
    this.firstName = "John";
    this.lastname = "Doe";
}

var person = new Person();
person.age = 25;
console.log(person);

var person1 = new Person();
console.log(person1);

// konstruktor
function User(userName, lastName) {
    this.firstName = userName;
    this.lastName = lastName;
    this.fullName = function ReturnFullName() {
        return this.firstName + ' ' + this.lastName;
    }
}

var user = new User('Vedran', 'Vukovic');
console.log(user);

var user1 = new User('Pero', 'Peric');
console.log(user1.fullName());

const ime = 'Vedran';
const pozdrav = `Moje ime je ${ime}`;
console.log(pozdrav);