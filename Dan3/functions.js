console.log("Functions");

function write() {
  return "Ispis teksta pomocu funkcije";
}

console.log(write());
var tekst = write();
console.log(tekst);
var opis = write() + " Lorem ipsum";

function sum(a, b) {
  return a + b;
}

// ES6 only
function sum(a, b = 15) {
  return a + b;
}

var a = sum(5, 5);
console.log(a);

var b = sum(1);
console.log(b);

var addOne = function (value) {
  return value + 1;
};

console.log(addOne(5));
