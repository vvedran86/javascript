console.log("Arrays");

var names = ["John", "Jane", "Mike"];
console.log(names);
console.log(typeof names);

var name = names[2] != undefined ? names[2] : "";
console.log(name);

names[2] = "Mickey";
names[3] = "Holly";
names.push("Molly");
console.log(names);

var matrix = [
  [1, 2, 3],
  ["John", "Jane", "Mike"],
];

console.log(matrix);
console.log(matrix[1][2]);

var names = ["John", "Jane", , "Mike"];
console.log(names);

for (var i = 0; i < names.length; i++) {
  console.log(names[i]);
}

names.forEach(function (value) {
  console.log(value);
});
