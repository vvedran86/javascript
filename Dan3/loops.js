console.log("Loops");

var count = 0;
while (count < 10) {
  console.log(count);
  count++;
}

var count = 10;
do {
  console.log(count);
  count++;
} while (count < 10);

for (var i = 0; i < 10; i + 2) {
  console.log(i);
}

var text = "";
for (var i = 0; i < 10; i++) {
  if (i === 3) {
    break;
  }
  text += "The number is" + i + "\n";
}

console.log(text);
