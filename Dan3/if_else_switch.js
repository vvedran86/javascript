var a = 10;
var b = 15;

// Single IF
if (a < b) {
  console.log("varijabla a je manja od varijable b");
}

// Single IF else
if (a > b) {
  console.log("varijabla a je veca od varijable b");
} else {
  console.log("varijabla a je manja od varijable b");
}

// Multiple IF
if (1 > 2) {
  console.log("Prvi IF");
}
if (1 > 2) {
  console.log("Drugi IF");
}
if (1 > 2) {
  console.log("Treci IF");
}

// If else if
if (1 < 2) {
  console.log("Prvi if else");
} else if (1 < 2) {
  console.log("Drugi if else");
}

// If else if else
if (1 > 2) {
  console.log("Prvi if else");
} else if (1 > 2) {
  console.log("Drugi if else");
} else {
  console.log("Treci if else");
}

var name = "Pero";

switch (name) {
  case "Mirko":
    console.log("Miocic");
    break;
  case "Pero":
    console.log("Peric");
    break;
  default:
    console.log("Tko je taj???");
    break;
}

var x = 5;
var y = 10;
var z = 15;

if ((x < y && y < z) || (x > y && y > z)) {
  console.log("Y je izmedju X i Z");
}
