"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * Todo object
 * @constructor
 */
var ToDoNote = /*#__PURE__*/function () {
  function ToDoNote() {
    var _this = this;

    _classCallCheck(this, ToDoNote);

    this.saveItemList = function () {
      var listItems = document.querySelectorAll('#todoList li');
      var listItemsArr = Array.from(listItems);
      var listItemList = listItemsArr.map(function (item) {
        return [item.childNodes[0].checked, item.childNodes[1].nodeValue];
      });
      var listItemListJSON = JSON.stringify(listItemList); // spremanje u Local storage (browser)

      _this.saveJSONIntoLocalStorage(listItemListJSON);
    };

    this.markDone = function (event, element) {
      if (event) {
        var item = event.target;
        item.parentNode.classList.toggle('done');
        item.nextSibling.nextSibling.disabled = item.checked;
      } else if (element) {
        element[1] ? element[0].classList.add('done') : false;
        element[0].childNodes[0].checked = element[1]; //param element je array

        element[0].childNodes[2].disabled = element[1];
        console.log(element); // childNodes[2] je gumb
      }
    };

    this.removeItem = function (event) {
      var removeButton = event.target;
      removeButton.parentNode.remove();

      _this.checkItemsIfExist();
    };

    this.addItem = function () {
      // Dohvati vrijednost input polja
      var textValue = inputTask.value;

      if (!textValue) {
        alert('Upisi task');
        return;
      } // Kreiraj novi element


      var item = _this.createItem(textValue); // Umetni element u listu


      _this.todoList.appendChild(item); // Provjeri listu


      _this.checkItemsIfExist(); // Resetiraj input listu


      _this.inputTask.value = '';
    };

    this.inputTask = document.querySelector('#inputTask');
    this.addButton = document.querySelector('#addTask');
    this.todoList = document.querySelector('#todoList');
    this.saveList = document.querySelector('#saveTaskList'); //this.addItem = this.addItem.bind(this);
  }

  _createClass(ToDoNote, [{
    key: "checkItemsIfExist",
    value: function checkItemsIfExist(checked) {
      var listItems = document.querySelectorAll('#todoList li');

      if (listItems.length > 0) {
        this.saveList.disabled = checked;
        checked != undefined ? this.setCheckboxCheckedStatus(listItems, checked) : false;
      } else {
        this.saveList.disabled = true;
      }
    }
  }, {
    key: "saveJSONIntoLocalStorage",
    value: function saveJSONIntoLocalStorage(json) {
      if (Storage != NaN || Storage != undefined) {
        // cisto blesava provjera za stari browser
        localStorage.tasksList = json;
      } else {
        alert('Imas prestari browser, ajd bok!');
      }
    }
  }, {
    key: "addCheckbox",
    value: function addCheckbox(item) {
      var checkbox = document.createElement('input');
      checkbox.setAttribute('type', 'checkbox');
      checkbox.addEventListener('click', this.markDone);
      item.insertBefore(checkbox, item.firstChild);
    }
  }, {
    key: "addRemoveButton",
    value: function addRemoveButton(item) {
      var removeButton = document.createElement('button');
      removeButton.className = 'removeButton';
      removeButton.innerText = 'remove';
      removeButton.addEventListener('click', this.removeItem);
      item.appendChild(removeButton);
    }
  }, {
    key: "createItem",
    value: function createItem(text) {
      var item = document.createElement('li');
      item.innerText = text;
      this.addCheckbox(item);
      this.addRemoveButton(item);
      return item;
    }
  }, {
    key: "loadFromLocalStorage",
    value: function loadFromLocalStorage(tasksList) {
      var _this2 = this;

      var listItems = JSON.parse(tasksList);
      listItems.forEach(function (element) {
        console.log(element); // Kreiraj novi element

        var item = _this2.createItem(element[1]);

        _this2.markDone(false, [item, element[0]]);

        _this2.todoList.appendChild(item);
      });
      this.checkItemsIfExist();
    }
  }, {
    key: "checkLocalStorage",
    value: function checkLocalStorage() {
      if (localStorage.tasksList) {
        this.loadFromLocalStorage(localStorage.tasksList);
      }
    }
  }, {
    key: "addListeners",
    value: function addListeners() {
      this.addButton.addEventListener('click', this.addItem);
      this.saveList.addEventListener('click', this.saveItemList);
    }
  }, {
    key: "init",
    value: function init() {
      this.addListeners();
      this.checkLocalStorage();
    }
  }]);

  return ToDoNote;
}();

;
var todo = new ToDoNote();
window.addEventListener('load', todo.init());
