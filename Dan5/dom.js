var desc = document.querySelector('.description');
var content = desc.innerHTML;
var contentText = desc.textContent;
var contentText2 = desc.innerText;

console.log(content);
console.log(contentText);
console.log(contentText2);

var img = document.querySelector('img');
setTimeout(function() {
    img.src = 'team.jpg';
}, 3000);

var width = img.getAttribute('width');
// img.setAttribute('width', 250);
// img.removeAttribute('height');

var name = img.dataset.ime;

setTimeout(function() {
    console.log(window.pageYOffset);
}, 3000);

img.scrollLeft = 100;
img.scrollTop = 100;

var x = img.scrollLeft;
var y = img.scrollTop;

console.log(x, y);
console.log(img.scrollLeft, img.scrollTop);

desc.style.height = '200px';
desc.style.overflow = 'auto';

setTimeout(function() {
    img.classList.add('show');
    img.classList.remove('hide');
}, 3000);


var styles = window.getComputedStyle(img, null);
var style = styles.getPropertyValue('width');
console.log(style);

var el = document.querySelector('.cube');
var pos = 0;

function frame() {
    if (pos > 1 && pos < 1000) {
        pos++;
        el.style.left = pos + 'px';
    }
    else if (pos === 1000) {
        pos--
        el.style.right = pos + 'px';
    }
    else if (pos === 999) {
        pos--
        el.style.right = pos + 'px';
    }
}

setInterval(frame, 10);