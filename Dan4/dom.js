var idSelektor = document.getElementById('footer');
console.log(idSelektor);
var querySelektor = document.querySelector('#footer');
console.log(querySelektor);

// querySelektor.innerHTML = '<div class="info">Info:</div>';

var description = document.querySelector('.description span');
var text = description.innerHTML.replace('4 rijeke', '10 rijeka');
description.innerHTML = text;

var info = document.createElement('div');
info.setAttribute('class', 'info');
info.innerText = 'Info:';
querySelektor.appendChild(info);
console.log(info);

var desc = document.querySelector('.description');
desc.removeChild(description);
