var emptyObj = {};
console.log(emptyObj);

var dog = {
    breed: 'Golden Retriver',
    bark: function () {
        return 'wuf Wuf';
    },
};

dog.age = 3;
dog.color = 'Gold';
dog.age = 2;

var isDeleted = delete dog.color;

if (isDeleted) {
    console.log(dog);
}

for (var key in dog) {
    if (typeof dog[key] !== 'function') {
        console.log('Key is ' + key + ', value is ' + dog[key]);
    }
}

var car = {
    color: 'Red',
    doors: 4,
};

console.log(car);

var json = JSON.stringify(car);
console.log(json);

var jParse = JSON.parse(json);
console.log(jParse);

var date = new Date();
console.log(date);

console.log(Math.PI);
console.log(Math.PI.toPrecision(4));
