console.log("Start data types");

var login;
var age = 45;
var euro = 7.55;
var desc = "Lorem ipsum dolor sit amet";
var status = true;

console.log(typeof login);
console.log(typeof age);
console.log(typeof euro);
console.log(typeof desc);
console.log(typeof status);

console.log("End data types");
