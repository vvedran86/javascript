console.log("Start operators");

console.log(5 % 2);
console.log(5 ** 2);

var x = 5;
x += 3;
console.log(x);

var y = 2;
y++;
console.log(y);

var a = 5;
var b = a++;
console.log(a, b);

console.log("5" == 5);
console.log("5" === 5);

console.log(!true);

console.log(true || true);

var p = true;
var q = true;

console.log(p === q);

console.log("End operators");
