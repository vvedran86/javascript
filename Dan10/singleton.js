// Single import
// import { sum } from './math.js';

// Alias import
// import { sum as addAll } from './math.js';

// Namespace import (preporuka)
import * as MathLib from './math.js';

console.log(MathLib.sum(1,2,3,4));

function Pero(name) {
    this.name = name;
};

const Singleton = (function() {
    let instance;

    return {
        getInstance: function (name) {
            if (!instance) {
                instance = new Pero(name);
            }
            return instance;
        }
    }
})();

let obj1 = new Singleton.getInstance('Pero');
let obj2 = new Singleton.getInstance('Ana');

console.log(obj1 === obj2);
console.log(obj1.name);
console.log(obj2.name);