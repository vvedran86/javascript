export const PI = 3.14;

export function sum(...args) {
    log('sum', args);
    return args.reduce((num, total) => total + num);    
};

function log(...msg) {
    console.log(...msg);
};

// let num = [120,55,33];

// let test = num.reduce(function(total, num) {
//     return total - num;
// });

// console.log(test);
//export {PI, sum};